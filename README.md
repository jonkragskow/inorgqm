# inorqm

`inorgqm` is a python module for working performing quantum mechanical calculations using phenomenological spin operators

# Installation

For convenience, `inorgqm` is available on [PyPI](https://pypi.org/project/inorgqm/) and so can be installed using `pip`

```
pip install inorgqm
```

first import it into your python file

```
import inorgqm.multielectron as me
```

and then call them as 

```
me.function_name(arguments)
```

To find information on any of the functions included in `inorgqm`, use the `help` command from within a python environment, e.g.

```
help(me.function_name)
```
