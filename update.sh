#! /usr/bin/env bash

rm -r build dist *egg*
python3 -m build
python3 -m twine upload --repository pypi dist/*
